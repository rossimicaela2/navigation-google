# README #

# NAvegacion SDK Google

_Navegacion en tiempo real; donde se dibuja el trayecto del usuario. Se le da la posibilidad de guardar esa ruta en BBDD del dispositivo_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos 📋

_Xcode 12 versio iOS 14_ 
_API KEY Google SDK : configurar una propia y modificar el archivo SceneDelegate.swift -  


## Versionado 📌

1.0.0
