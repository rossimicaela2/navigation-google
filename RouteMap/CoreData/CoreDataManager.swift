//
//  CoreDataManager.swift
//  RouteMap
//
//  Created by Micaela Rossi on 28/09/2021.
//

import Foundation
import CoreData
import GoogleMaps


struct RouteUser {
    var path: GMSMutablePath
    var time: Int
    var distance: Double
    var date: Date
    var id: Int
}

class CoreDataManager{
    
    private let modelName = "RouteUser"
    private let entityName = "Route"
    
    //2
    private let container : NSPersistentContainer!
    //3
    init() {
        container = NSPersistentContainer(name: modelName)
        
        setupDatabase()
    }
    
    
    private func setupDatabase() {
        //4
        container.loadPersistentStores { (desc, error) in
            if let error = error {
                print("Error loading store \(desc) — \(error)")
                return
            }
            print("Database ready!")
        }
    }
    
    func save (){
        do {
            try container.viewContext.save()
        }catch{
            print("Error info: \(error)")
        }
    }
    
    func deleteRoute(_ id: Int){
        let context = container.viewContext
        let request : NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entityName)
        let coreData_items  = (try? context.fetch(request) as? [NSManagedObject]) ?? []
        for entity in coreData_items {
            let idEntity = entity.value(forKey: "id") as! Int
            if (idEntity == id){
                context.delete(entity)
            }
        }
        do {
            try context.save()
        }catch {
            print("Error info: \(error)")
        }
    }
    
    func saveRoute (path: GMSPath, time: Int)  {
        
        let context = container.viewContext
        let request : NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entityName)
        let coreData_items  = (try? context.fetch(request) as? [NSManagedObject]) ?? []
        
        var coords: [Dictionary<String, AnyObject>] = []
        for i in 0..<path.count() {
            let latitude = NSNumber(value: path.coordinate(at: i).latitude)
            let longitude = NSNumber(value: path.coordinate(at: i).longitude)
            let coord = ["latitude" : latitude, "longitude" : longitude]
            coords.append(coord)
        }
        
        let total: Double = 0.0 + getDistance(from: path.coordinate(at: 0), to: path.coordinate(at: path.count() - 1))
        let id = coreData_items.count + 1
        
        if let routeEntity = NSEntityDescription.entity(forEntityName: entityName, in: context) {
            let route = NSManagedObject(entity: routeEntity, insertInto: context)
            route.setValue(coords, forKey: "coordinates")
            route.setValue(total, forKey: "distance")
            route.setValue(NSDate(), forKey: "date")
            route.setValue(time, forKey: "time")
            route.setValue(id, forKey: "id")
            self.save()
        }
    }
    
    func getDistance(from: CLLocationCoordinate2D, to: CLLocationCoordinate2D) -> CLLocationDistance {
        let from = CLLocation(latitude: from.latitude, longitude: from.longitude)
        let to = CLLocation(latitude: to.latitude, longitude: to.longitude)
        return from.distance(from: to) / 1000 // save in km
    }
    
    func getRoutes () -> [RouteUser] {
        let context = container.viewContext
        let request : NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entityName)
        let coreData_items  = (try? context.fetch(request) as? [NSManagedObject]) ?? []
        
        var allResult : [RouteUser] = []
        for entity in coreData_items {
            let  coordinates = entity.value(forKey: "coordinates") as! [Dictionary<String, AnyObject>]
            let  date = entity.value(forKey: "date") as! Date
            let  distance = entity.value(forKey: "distance") as! Double
            let  time = entity.value(forKey: "time") as! Int
            let  id = entity.value(forKey: "id") as! Int
            
            let pathResult : GMSMutablePath = GMSMutablePath()
            for item in coordinates {
                if let latitude = item["latitude"]?.doubleValue,
                   let longitude = item["longitude"]?.doubleValue {
                    pathResult.addLatitude(latitude, longitude: longitude)
                }
            }
            let newRoute = RouteUser(path: pathResult, time: time, distance: distance, date: date, id: id)
            allResult.append(newRoute)
        }
        return allResult
    }
    
}
