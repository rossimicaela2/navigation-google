//
//  DetailRouteViewController.swift
//  RouteMap
//
//  Created by Micaela Rossi on 30/09/2021.
//

import Foundation
import UIKit
import GoogleMaps
import MapKit
import CoreData

class DetailRouteViewController: UIViewController {
    
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    
    public var routeUser: RouteUser?
    private var polyline = GMSPolyline()
    private let btnShare:UIButton = UIButton()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(false)
        self.loadDetailRoute()
        self.loadDetail()
    }
    
    
    func loadDetailRoute(){
        if let path = routeUser?.path {
            self.polyline.strokeColor = UIColor.blue
            self.polyline.strokeWidth = 5.0
            self.polyline.map = self.mapView
            self.polyline.path = path
            let marker = GMSMarker(position: path.coordinate(at: path.count() - 1))
            marker.icon = GMSMarker.markerImage(with: UIColor.black)
            marker.map = self.mapView
            let markerInit = GMSMarker(position: path.coordinate(at: 0))
            markerInit.icon = GMSMarker.markerImage(with: UIColor.systemYellow)
            markerInit.map = self.mapView
            let camera = GMSCameraPosition.camera(withTarget: path.coordinate(at: 0), zoom: 17.0)
            self.mapView.animate(to: camera)
        }
    }
    
    func loadDetail(){
        guard let date = routeUser?.date, let distance = routeUser?.distance, let time = routeUser?.time else {
            return
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let dateString = dateFormatter.string(from: date as Date)
        self.dateLbl.text = "Fecha: \(dateString)"
        self.distanceLbl.text = "Distancia: \(String(distance)) km"
        let secondsTime = secondsToHoursMinutesSeconds(seconds: time)
        self.timeLbl.text = "Tiempo: \(String(secondsTime))"
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> String {
        return "\(String(format: "%02d", seconds / 3600)) : \(String(format: "%02d", (seconds % 3600) / 60)) : \(String(format: "%02d", ((seconds % 3600) % 60)))"
    }
    
    func createButton(){
        
        if let image = UIImage(systemName: "square.and.arrow.up") {
            btnShare.setImage(image, for: .normal)
        }
        
        //btnShare.setTitle("Compartir", for: UIControl.State.normal)
        btnShare.setTitleColor(UIColor.white, for: UIControl.State.normal)
        btnShare.backgroundColor = UIColor.white
        btnShare.layer.cornerRadius = 25
        
        btnShare.layer.borderWidth = 1
        btnShare.layer.borderColor = UIColor.lightGray.cgColor
        
        btnShare.translatesAutoresizingMaskIntoConstraints = false
        
        btnShare.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        self.view.addSubview(btnShare)
        
        btnShare.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).isActive = true
        
        btnShare.bottomAnchor.constraint(equalTo: self.view.layoutMarginsGuide.bottomAnchor, constant: -10).isActive = true
        
        
        btnShare.widthAnchor.constraint(equalToConstant: 50).isActive = true
        btnShare.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        btnShare.addTarget(self, action: #selector(shareImageButton), for: UIControl.Event.touchDown)
    }
    
    @IBAction func shareImageButton(_ sender: UIButton) {
        
        let image = self.view.snapshot
        
        let imageToShare = [ image! ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
}


extension UIView {
    var snapshot: UIImage? {
        UIGraphicsBeginImageContext(self.frame.size)
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        layer.render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}
