//
//  TabBar.swift
//  RouteMap
//
//  Created by Micaela Rossi on 29/09/2021.
//

import Foundation
import UIKit

class TabBar:  UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        UITabBar.appearance().barTintColor = .systemBackground
        tabBar.tintColor = .label
        setupVCs()
    }
    
    func setupVCs() {
            viewControllers = [
                createNavController(for: NavigationRouteViewController(), title: NSLocalizedString("Navegación", comment: ""), image: UIImage(systemName: "house")!),
                createNavController(for: ListRoutesViewController(), title: NSLocalizedString("Rutas", comment: ""), image: UIImage(systemName: "magnifyingglass")!),
            ]
        }

    fileprivate func createNavController(for rootViewController: UIViewController,
                                                      title: String,
                                                      image: UIImage) -> UIViewController {
            let navController = UINavigationController(rootViewController: rootViewController)
            navController.tabBarItem.title = title
            navController.tabBarItem.image = image
            navController.navigationBar.prefersLargeTitles = true
            rootViewController.navigationItem.title = title
            return navController
        }
    
}
