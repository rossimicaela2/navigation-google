//
//  NavigationRouteViewController.swift
//  RouteMap
//
//  Created by Micaela Rossi on 29/09/2021.
//


import UIKit
import GoogleMaps
import MapKit
import CoreData

class NavigationRouteViewController: UIViewController, GMSMapViewDelegate {
    
    @IBOutlet weak var mapView: GMSMapView!
    
    var locationManager: CLLocationManager = CLLocationManager()
    var points: [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
    
    private var locations = [CLLocation]()
    private var path = GMSMutablePath()
    private var polyline = GMSPolyline()
    private var currentPositionMarker = GMSMarker()
    private var isFirstMessage = true
    private var rec = false
    
    let btnAlertActionSheet:UIButton = UIButton()
    
    var timer : Timer?
    var startTime = 0
    var timerReset = true
    
    let routeManager = CoreDataManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        self.config()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func config(){
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        locationManager.startUpdatingLocation()
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
    }
    
    func createButton(){
        
        btnAlertActionSheet.setTitle("Grabar", for: UIControl.State.normal)
        btnAlertActionSheet.setTitleColor(UIColor.white, for: UIControl.State.normal)
        btnAlertActionSheet.backgroundColor = UIColor.systemGreen
        btnAlertActionSheet.layer.cornerRadius = 25
        
        btnAlertActionSheet.layer.borderWidth = 1
        btnAlertActionSheet.layer.borderColor = UIColor.white.cgColor
        
        btnAlertActionSheet.translatesAutoresizingMaskIntoConstraints = false
        
        btnAlertActionSheet.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        self.view.addSubview(btnAlertActionSheet)
        
        let btnTopAnchor = btnAlertActionSheet.bottomAnchor.constraint(equalTo: self.view.layoutMarginsGuide.bottomAnchor, constant: -10)
        btnTopAnchor.isActive = true
        
        let btnCenterXAnchor = btnAlertActionSheet.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
        btnCenterXAnchor.isActive = true
        
        btnAlertActionSheet.widthAnchor.constraint(equalToConstant: 110).isActive = true
        btnAlertActionSheet.heightAnchor.constraint(equalToConstant: 70).isActive = true
        
        btnAlertActionSheet.addTarget(self, action: #selector(processBtnEvent), for: UIControl.Event.touchDown)
    }
    
    
    @objc func updateTime(){
        if timerReset {
            startTime = 0
        }
        startTime += 1
        let (h,m,s) = secondsToHoursMinutesSeconds(seconds: Int(startTime))
        let transform =  "\(String(format: "%02d", h)) : \(String(format: "%02d", m)) : \(String(format: "%02d", s))"
        btnAlertActionSheet.setTitle(transform, for: .normal)
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    @objc func processBtnEvent(src:UIButton, event:UIControl.Event){
        if src.titleLabel!.text == "Grabar" {
            rec = true
            btnAlertActionSheet.setTitle("Parar", for: .normal)
            btnAlertActionSheet.backgroundColor = .red
            timerReset = false // reset
            guard timer == nil else { return }
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        }
        else {
            rec = false
            let marker = GMSMarker(position: path.coordinate(at: path.count() - 1))
            marker.icon = GMSMarker.markerImage(with: UIColor.black)
            marker.map = self.mapView
            locationManager.stopUpdatingLocation()
            timer!.invalidate()
            btnAlertActionSheet.setTitle("Grabar", for: .normal)
            btnAlertActionSheet.backgroundColor = UIColor.systemGreen
            createAlert()
        }
    }
    
    func createAlert(){
        
        let alertActionSheetController: UIAlertController = UIAlertController(title: "", message: "¿Desea grabar la ruta?", preferredStyle: UIAlertController.Style.actionSheet)
        
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (cancelAction) in
            self.path.removeAllCoordinates()
            self.mapView.clear()
            self.isFirstMessage = true
            self.locationManager.startUpdatingLocation()
        }
        
        let okAction:UIAlertAction = UIAlertAction(title: "Grabar", style: UIAlertAction.Style.default) { [self] (okAction) in
            routeManager.saveRoute(path: path, time: Int(startTime))
            path.removeAllCoordinates()
            mapView.clear()
            isFirstMessage = true
            locationManager.startUpdatingLocation()
        }
        
        alertActionSheetController.addAction(cancelAction)
        alertActionSheetController.addAction(okAction)
        
        self.present(alertActionSheetController, animated: true)
    }
    
    
}

// Delegates to handle events for the location manager.
extension NavigationRouteViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
        locationManager.startUpdatingLocation()
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didFailWithError error: Error) {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let location = locations.last else {
            return
        }
        
        if (self.isFirstMessage && rec) {
            self.initializePolylineAnnotation()
            self.isFirstMessage = false
            let marker = GMSMarker(position: location.coordinate)
            marker.icon = GMSMarker.markerImage(with: UIColor.systemYellow)
            marker.map = self.mapView
        }
        
        if (rec){
            self.points.append(location.coordinate)
            self.updateOverlay(currentPosition: location)
        }
        self.updateMapFrame(newLocation: location, zoom: 17.0)
        self.updateCurrentPositionMarker(currentLocation: location)
        
    }
    
    func initializePolylineAnnotation() {
        self.polyline.strokeColor = UIColor.blue
        self.polyline.strokeWidth = 5.0
        self.polyline.map = self.mapView
    }
    
    func updateOverlay(currentPosition: CLLocation) {
        path.addLatitude(currentPosition.coordinate.latitude, longitude: currentPosition.coordinate.longitude)
        self.polyline.path = self.path
    }
    
    func updateMapFrame(newLocation: CLLocation, zoom: Float) {
        let camera = GMSCameraPosition.camera(withTarget: newLocation.coordinate, zoom: zoom)
        self.mapView.animate(to: camera)
    }
    
    func updateCurrentPositionMarker(currentLocation: CLLocation) {
        self.currentPositionMarker.map = nil
        self.currentPositionMarker = GMSMarker(position: currentLocation.coordinate)
        self.currentPositionMarker.icon = GMSMarker.markerImage(with: UIColor.red)
        self.currentPositionMarker.map = self.mapView
    }
    
}
