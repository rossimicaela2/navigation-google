//
//  ListRoutesViewController.swift
//  RouteMap
//
//  Created by Micaela Rossi on 29/09/2021.
//

import Foundation
import UIKit
import GoogleMaps
import MapKit
import CoreData

class ListRoutesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    let routeManager = CoreDataManager()
    var routes : [RouteUser] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(false)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        loadRoutes()
    }
    
    func loadRoutes(){
        routes = self.routeManager.getRoutes()
        
        let cellNib = UINib(nibName: "RouteCellViewController", bundle: nil)
        self.tableView.register(cellNib, forCellReuseIdentifier: "RouteCellViewController")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.routes.count
        }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cellIdentifier = "RouteCellViewController"

            guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? RouteCellViewController  else {
                fatalError("The dequeued cell is not an instance of UserTableViewCell.")
            }
        
        let date = routes[indexPath.row].date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let dateString = dateFormatter.string(from: date as Date)
        cell.setName(name: "Ruta \(dateString)")

            return cell
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextVC = DetailRouteViewController()
        nextVC.routeUser = routes[indexPath.row]
        navigationController?.pushViewController(nextVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            if let idRoute = routes[indexPath.row].id as Int? {
                routeManager.deleteRoute(idRoute)
                routes.remove(at: indexPath.row)
                tableView.reloadData()
            }
        }
    }
    
   
}
