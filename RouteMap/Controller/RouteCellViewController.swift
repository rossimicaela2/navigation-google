//
//  RouteCellViewController.swift
//  RouteMap
//
//  Created by Micaela Rossi on 29/09/2021.
//

import Foundation
import UIKit

class RouteCellViewController: UITableViewCell {
    
    
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
 
    func setName(name: String) {
        self.name.text = name
    }
}
